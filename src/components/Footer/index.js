import React from "react";

const Footer = () => {
  return (
    <footer className="text-center">
      <p className="text-dark">Powered by Unknown</p>
    </footer>
  );
};

export default Footer;

const styles = {
  footer: {
    height: "1rem",
    position: "absolute",
    bottom: 0,
    width: "100%",
  },
};
