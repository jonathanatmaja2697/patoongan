import React from "react";
import { ScaleLoader } from "react-spinners";
import { Backdrop } from "@mui/material";

const Loading = ({ loading }) => {
  return (
    <Backdrop
      open={loading}
      invisible
      transitionDuration={{ enter: 500, exit: 500 }}
    >
      <ScaleLoader />
    </Backdrop>
  );
};

export default Loading;
