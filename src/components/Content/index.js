import React from "react";

const Content = ({ children, centered = false }) => {
  return <div style={styles.container(centered)}>{children}</div>;
};

export default Content;

const styles = {
  container: (centered) => ({
    flex: 1,
    flexDirection: "column",
    display: "flex",
    padding: 20,
    justifyContent: centered ? "center" : null,
    width: "100%",
  }),
};
