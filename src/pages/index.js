import { Button, TextField } from "@mui/material";
import { useDispatch } from "react-redux";
import {
  Container,
  Content,
  Footer,
  Gap,
  Header,
  Loading,
} from "../components";
import { Colors } from "../enums";
import { hp } from "../functions";
import { dismissLoading, login, showLoading } from "../redux/actions";

const Home = () => {
  const dispatch = useDispatch();
  const onSubmit = (e) => {
    e.preventDefault();
    const { email, password } = e.target;
    dispatch(login(email.value, showLoading, dismissLoading));
  };
  return (
    <Container>
      <Header />
      <Content centered>
        <img src="/assets/front-illustration.png" style={styles.image} />
        <form onSubmit={onSubmit}>
          <div style={{ flexDirection: "column", display: "flex" }}>
            <TextField
              id="email"
              variant="outlined"
              type="email"
              label="Email"
              inputMode="email"
              name="email"
            />
            <Gap height={hp(2)} />
            <TextField
              id="password"
              variant="outlined"
              type="Password"
              label="password"
              name="password"
            />
            <Gap height={hp(2)} />
            <Button
              variant="contained"
              size="large"
              style={{ backgroundColor: Colors.colors.blue }}
              disableElevation
              type="submit"
            >
              Lanjut
            </Button>
            <Gap height={hp(2)} />
            <p className="text-center">Lupa password?</p>
          </div>
        </form>
      </Content>
      <Footer />
    </Container>
  );
};
export default Home;

const styles = {
  image: {
    width: "50vh",
    alignSelf: "center",
  },
};
