import React, { useEffect } from "react";
import { Provider, useSelector } from "react-redux";
import withRedux, { createWrapper } from "next-redux-wrapper";
import store from "../../src/redux";
import "bootstrap/dist/css/bootstrap.css";
import { Loading } from "../components";

function MyApp({ Component, pageProps }) {
  const { loading } = useSelector((state) => state.data);

  return (
    <Provider store={store}>
      <Loading loading={loading} />
      <Component {...pageProps} />
    </Provider>
  );
}
const makestore = () => store;
const wrapper = createWrapper(makestore);

export default wrapper.withRedux(MyApp);
