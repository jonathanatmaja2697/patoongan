import axios from "axios";
import {
  GET_KYC_LIST,
  SET_LOADING,
  SET_DISMISS_LOADING,
} from "../action-types";

const setKycList = (payload) => ({
  type: GET_KYC_LIST,
  payload,
});

const showLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

const dismissLoading = (payload) => ({
  type: SET_DISMISS_LOADING,
  payload,
});

const getKycList =
  (showLoading, dismissLoading, showAlertError) => (dispatch) => {
    return new Promise((resolve, reject) => {
      showLoading && dispatch(showLoading());
      dispatch(
        setKycList([
          {
            id: 1,
            name: "Jonathan Atmaja",
            zonid: 92019201,
            status: "WAITING",
          },
          {
            id: 2,
            name: "Irene",
            zonid: 92019201,
            status: "WAITING",
          },
          {
            id: 3,
            name: "YELESIA",
            zonid: 92019201,
            status: "APPROVED",
          },
          {
            id: 3,
            name: "YELESIA",
            zonid: 92019201,
            status: "APPROVED",
          },
          {
            id: 3,
            name: "YELESIA",
            zonid: 92019201,
            status: "APPROVED",
          },
          {
            id: 3,
            name: "YELESIA",
            zonid: 92019201,
            status: "APPROVED",
          },
          {
            id: 3,
            name: "YELESIA",
            zonid: 92019201,
            status: "APPROVED",
          },
          {
            id: 3,
            name: "YELESIA",
            zonid: 92019201,
            status: "APPROVED",
          },
          {
            id: 3,
            name: "YELESIA",
            zonid: 92019201,
            status: "APPROVED",
          },
          {
            id: 3,
            name: "YELESIA",
            zonid: 92019201,
            status: "APPROVED",
          },
          {
            id: 3,
            name: "YELESIA",
            zonid: 92019201,
            status: "APPROVED",
          },
          {
            id: 3,
            name: "YELESIA",
            zonid: 92019201,
            status: "APPROVED",
          },
        ])
      );
      resolve([
        {
          id: 1,
          name: "Jonathan Atmaja",
          status: "WAITING",
        },
        {
          id: 2,
          name: "Irene",
          status: "WAITING",
        },
        {
          id: 3,
          name: "YELESIA",
          status: "APPROVED",
        },
      ]);
    });
  };

const login = (request, showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    axios
      .get("/api/account/login?email=" + request)
      .then((res) => {
        resolve(res.data);
      })
      .catch((err) => {
        reject(err);
        console.log(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

export { getKycList, login, showLoading, dismissLoading };
